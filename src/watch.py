import time
import logging
import yaml
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from connectors import BDD_sqlite3


def init(cmd_args):
    print("Watchdog launch")
    config = read_config()

    patterns = cmd_args.p if hasattr(cmd_args, "p") else "*.py"
    interval = int(cmd_args.interval) if cmd_args.interval \
        else config["interval"]

    ignore_patterns = [".git", "./.idea/"]
    ignore_directories = True
    case_sensitive = True
    my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns,
                                                   ignore_directories,
                                                   case_sensitive)

    my_event_handler.on_created = on_created
    my_event_handler.on_deleted = on_deleted
    my_event_handler.on_modified = on_modified
    my_event_handler.on_moved = on_moved

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    path = "."
    go_recursively = True
    my_observer = Observer()
    my_observer.schedule(my_event_handler, path, recursive=go_recursively)

    my_observer.start()
    try:
        while True:
            time.sleep(interval)
    except KeyboardInterrupt:
        my_observer.stop()
        my_observer.join()


def on_created(event):
    log = f"created: {event.src_path}"
    BDD_sqlite3.insert(log)


def on_deleted(event):
    log = f"deleted: {event.src_path}"
    BDD_sqlite3.insert(log)


def on_modified(event):
    log = f"modified: {event.src_path}"
    BDD_sqlite3.insert(log)


def on_moved(event):
    log = f"ok ok ok, someone moved {event.src_path} to {event.dest_path}"
    BDD_sqlite3.insert(log)


def read_config():
    with open("config.yaml", 'r') as stream:
        try:
            return yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
